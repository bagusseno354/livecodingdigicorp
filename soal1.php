<?php

function generateRandomString($length = 10) 
{
    return substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)) )),1,$length);
}


$tokens = [];

function generateToken($username)
{
	global $tokens;
    
	$newToken = generateRandomString();
    
    if(!is_array($tokens[$username]))
    	$tokens[$username] = [];
        
  	if(sizeof($tokens[$username]) >= 10)
    	array_shift($tokens[$username]);
        
  	array_push($tokens[$username], $newToken);
    
    return $newToken;
}

function verifyToken($username, $token)
{
	global $tokens;
    
    $found = false;
    
    for($i = 0; $i < sizeof($tokens[$username]); $i++)
    {
    	if($tokens[$username][$i] == $token)
        {
        	$found = true;
            array_splice($tokens[$username], $i, 1);
        
        	break;
        }
    }
}

// test
$token1 = generateToken('bagus');

var_dump($tokens);

echo '<br>';

generateToken('bagus');

var_dump($tokens);

echo '<br>';

verifyToken('bagus', $token1);

var_dump($tokens);

echo '<br>';

generateToken('bagus');
generateToken('bagus');
generateToken('bagus');
generateToken('bagus');
generateToken('bagus');
generateToken('bagus');
generateToken('bagus');
generateToken('bagus');
generateToken('bagus');
generateToken('bagus');

var_dump($tokens);

echo '<br>';


