<?php

function maxCharInString($string)
{
	$chars = [];
    
    foreach (str_split($string) as $char)
    {
    	if(!isset($chars[$char]))
        	$chars[$char] = 1;
      	else
        	$chars[$char] += 1;
    }
    	
  	$maxChar = array_keys($chars)[0];
    
    foreach($chars as $key => $val)
    {
    	if($val > $chars[$maxChar])
        	$maxChar = $key;
    }
        
    echo $maxChar . ' ' . $chars[$maxChar] . 'x';
}

maxCharInString("strawberry");