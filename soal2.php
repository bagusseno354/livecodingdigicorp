<?php

class Siswa
{
	public $nrp;
    public $nama;
    public $daftarNilai = [];
    
    function __construct($nrp, $nama, $daftarNilai)
    {
    	$this->nrp = $nrp;
        $this->nama = $nama;
        $this->daftarNilai = $daftarNilai;
    }
}

class Nilai
{
	public $mapel;
    public $nilai;
    
    function __construct($mapel, $nilai)
    {
    	$this->mapel = $mapel;
        $this->nilai = $nilai;
    }
}

function generateRandomString($length = 10) 
{
    return substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($length/strlen($x)) )),1,$length);
}


$mapel = new Nilai('Inggris', 100);

$siswa = new Siswa(1, 'Andi', [$mapel]);

$mapels = ['Inggris', 'Jepang', 'Indonesia'];

for($i = 0; $i < 10; $i++)
{
	$mapel = $mapels[array_rand($mapels, 1)];
	$nilai = new Nilai($mapel, rand(0, 100));
    
    new Siswa($i, generateRandomString(), [$nilai]); 
}